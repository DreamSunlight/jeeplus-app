import http from './interface'
const apiList = {
	paymoney:(data) => {
		return http.request({
			url: '/rest/busi/api/paymoney',
			method: 'GET',
			data
		})
	},
	appdown:(data) => {
		return http.request({
			url: '/rest/busi/api/appdown',
			method: 'POST',
			data
		})
	},
	getCode:(data) => {
		return http.request({
			url: '/rest/busi/api/appversion',
			method: 'POST',
			data
		})
	},
	// 获取验证码
	setCode: (data) => {
		return http.request({
			url: '/rest/busi/api/sendVerifyCode',
			method: 'POST',
			data
		})
	},
	// k线图月
	getMonthData: (data) => {
		return http.request({
			url: '/rest/busi/api/month',
			method: 'GET',
			data
		})
	},
	getDayData: (data) => {
		return http.request({
			url: '/rest/busi/api/day',
			method: 'GET',
			data
		})
	},
	// 动态配置
	config: (data) => {
		return http.request({
			url: '/rest/busi/api/config',
			method: 'GET',
			data
		})
	},
	// 注册
	register: (data) => {
		return http.request({
			url: '/rest/busi/api/register',
			method: 'POST',
			data
		})
	},
	// 登录
	login: (data) => {
		return http.request({
			url: '/rest/busi/api/login',
			method: 'POST',
			data
		})
	},
	getCodeImg: (data) => {
		return http.request({
			url: '/rest/sys/login/checkCode',
			method: 'GET',
			data
		})

	},
	// 找回密码
	setPwd: (data) => {
		return http.request({
			url: '/rest/busi/api/forgetPwd',
			method: 'POST',
			data
		})
	},
	// 找回密码
	resetPwd: (data) => {
		return http.request({
			url: '/rest/busi/api/resetPwd',
			method: 'POST',
			data
		})
	},
	// 首页
	homeIndex: (data) => {
		return http.request({
			url: '/rest/busi/api/attachment',
			method: 'GET',
			data
		})
	},
	// 首页分类
	getRecommendClassify: (data) => {
		return http.request({
			url: '/rest/busi/api/category',
			method: 'GET',
			data
		})
	},
	getClassify: (data) => {
		return http.request({
			url: '/rest/busi/api/category',
			method: 'GET',
			data
		})
	},
	// 首页分类
	getClassifyShops: (data) => {
		return http.request({
			url: '/rest/busi/api/goods',
			method: 'GET',
			data
		})
	},
	// 公告
	getNotice: (data) => {
		return http.request({
			url: '/rest/busi/api/notice',
			method: 'GET',
			data
		})
	},
	getNoticeDetail: (data) => {
		return http.request({
			url: '/rest/busi/api/noticeId',
			method: 'GET',
			data
		})
	},
	// 广告-会员报单
	getAdvertHy: (data) => {
		return http.request({
			url: '/rest/busi/api/attachment',
			method: 'GET',
			data
		})
	},
	// 付款方式
	payType: (data) => {
		return http.request({
			url: '/rest/busi/api/paytype',
			method: 'GET',
			data
		})
	},
	// 商品详情
	getShopDetails: (data) => {
		return http.request({
			url: '/rest/busi/api/goodsId',
			method: 'GET',
			data
		})
	},
	getDetailList: (data) => {
		return http.request({
			url: '/rest/busi/api/wallets',
			method: 'GET',
			data
		})
	},
	tageList: (data) => {
		return http.request({
			url: '/rest/busi/api/normal',
			method: 'GET',
			data
		})
	},
	// 加入购物车
	addShopCar: (data) => {
		return http.request({
			url: '/rest/busi/api/cart',
			method: 'POST',
			data
		})
	},
	// 购物车删除
	delShopCarNum: (data) => {
		return http.request({
			url: '/rest/busi/api/rcart',
			method: 'POST',
			data
		})
	},
	// 搜索
	seek: (data) => {
		return http.request({
			url: '/home/Shops/seek',
			method: 'POST',
			data
		})
	},
	ptransfer: (data) => {
		return http.request({
			url: '/rest/busi/api/ptransfer',
			method: 'GET',
			data
		})
	},
	myInfor: (data) => {
		return http.request({
			url: '/rest/busi/api/myInfo',
			method: 'GET',
			data
		})
	},
	// 个人中心
	my: (data) => {
		return http.request({
			url: '/rest/busi/api/myinvitcode',
			method: 'GET',
			data
		})
	},
	// 展示个人信息
	getInfo: (data) => {
		return http.request({
			url: '/rest/busi/api/myInfo',
			method: 'GET',
			data
		})
	},
	// 个人资料修改
	setInfo: (data) => {
		return http.request({
			url: '/rest/busi/api/myModify',
			method: 'POST',
			data
		})
	},
	// 我的地址
	getSite: (data) => {
		return http.request({
			url: '/rest/busi/api/myaddress',
			method: 'GET',
			data
		})
	},
	// 地址详情
	getSiteFind: (data) => {
		return http.request({
			url: '/rest/busi/api/addressId',
			method: 'POST',
			data
		})
	},
	// 新增地址
	addSite: (data) => {
		return http.request({
			url: '/rest/busi/api/address',
			method: 'POST',
			data
		})
	},
	// 编辑地址
	setSite: (data) => {
		return http.request({
			url: '/rest/busi/api/address',
			method: 'POST',
			data
		})
	},
	// 删除地址
	delSite: (data) => {
		return http.request({
			url: '/rest/busi/api/raddress',
			method: 'POST',
			data
		})
	},
	// 获取默认地址
	getDefaultSite: (data) => {
		return http.request({
			url: '/rest/busi/api/myaddress',
			method: 'GET',
			data
		})
	},
	// 我的钱包
	myMoneyGet: (data) => {
		return http.request({
			url: '/rest/busi/api/mywallet',
			method: 'GET',
			data
		})
	},
	// 充值页面信息获得
	getTopUpInfo: (data) => {
		return http.request({
			url: '/rest/busi/api/paymoney',
			method: 'GET',
			data
		})
	},
	// 充值申请
	addTopUp: (data) => {
		return http.request({
			url: '/rest/busi/api/recharge',
			method: 'POST',
			data
		})
	},
	// 充值记录
	getTopUpLog: (data) => {
		return http.request({
			url: '/rest/busi/api/precharge',
			method: 'GET',
			data
		})
	},
	// 提现申请
	addWithdraw: (data) => {
		return http.request({
			url: '/rest/busi/api/wichcash',
			method: 'POST',
			data
		})
	},
	// 提现申请
	addUwithdraw: (data) => {
		return http.request({
			url: '/rest/busi/api/uwichcash',
			method: 'POST',
			data
		})
	},
	// 兑现
	exchange: (data) => {
		return http.request({
			url: '/rest/busi/api/exchange',
			method: 'POST',
			data
		})
	},
	// 提现记录
	getWithdrawLog: (data) => {
		return http.request({
			url: '/rest/busi/api/pwichcash',
			method: 'GET',
			data
		})
	},
	// 购物车列表
	getShopCar: (data) => {
		return http.request({
			url: '/rest/busi/api/carts',
			method: 'GET',
			data
		})
	},
	// 购物车购买
	shopCarBuy: (data) => {
		return http.request({
			url: '/rest/busi/api/orders',
			method: 'POST',
			data
		})
	},
	// 我的团队
	myTeam: (data) => {
		return http.request({
			url: '/rest/busi/api/team',
			method: 'GET',
			data
		})
	},
	// 团队成员
	myTeamGet: (data) => {
		return http.request({
			url: '/rest/busi/api/myteam',
			method: 'GET',
			data
		})
	},
	// 下载APP背景图
	getConfigImg: (data) => {
		return http.request({
			url: '/rest/busi/api/appAndkef',
			method: 'GET',
			data
		})
	},
	// 支付订单商品获得
	getOrderShop: (data) => {
		return http.request({
			url: '/rest/busi/api/storage',
			method: 'POST',
			data
		})
	},
	// 支付订单商品获得 购物车
	getOrderShopCar: (data) => {
		return http.request({
			url: '/rest/busi/api/storage',
			method: 'POST',
			data
		})
	},
	// 立即购买
	shopPay: (data) => {
		return http.request({
			url: '/rest/busi/api/order',
			method: 'POST',
			data
		})
	},
	// 退出登录
	logina: (data) => {
		return http.request({
			url: '/rest/busi/api/out',
			method: 'POST',
			data
		})
	},
	// 注册协议
	getConfigStr: (data) => {
		return http.request({
			url: '/rest/busi/api/notice',
			method: 'GET',
			data
		})
	},
	// 订单列表
	getOrder: (data) => {
		return http.request({
			url: '/rest/busi/api/porder',
			method: 'GET',
			data
		})
	},
	// 确认收货
	yesOrder: (data) => {
		return http.request({
			url: '/rest/busi/api/confirmOrder',
			method: 'POST',
			data
		})
	},
	// 支付宝线上充值
	wxopenid: (data) => {
		return http.request({
			url: '/rest/busi/wxpay/weixin',
			method: 'GET',
			data
		})
	},
	// 支付宝线上充值
	alipayPay: (data) => {
		return http.request({
			url: '/rest/busi/alipay/pay',
			method: 'POST',
			data
		})
	},
	//微信线上充值
	wxPay: (data) => {
		return http.request({
			url: '/rest/busi/wxpay/pay',
			method: 'POST',
			data
		})
	},
	setPayFalse: (data) => {
		return http.request({
			url: '/rest/busi/alipay/notice',
			method: 'POST',
			data
		})
	},
	setPayTrue: (data) => {
		return http.request({
			url: '/rest/busi/alipay/order',
			method: 'POST',
			data
		})
	},
	huzhuan: (data) => {
		return http.request({
			url: '/home/my/huzhuan',
			method: 'GET',
			data
		})
	},
	huzhuan2: (data) => {
		return http.request({
			url: '/rest/busi/api/transfer',
			method: 'POST',
			data
		})
	},
	checkPayPwd: (data) => {
		return http.request({
			url: '/rest/busi/api/checkPayPwd',
			method: 'POST',
			data
		})
	}
}
export default apiList
